#!/usr/bin/env python

"""
Thermodynamics of the 1-d Heisenberg model. This is a nice warm-up, as the only
operators we need to track are the Hamiltonian and the right-most spin.
"""

import numpy as np
import scipy.special as sps

def operators(L,D,Jx,Jy,Jz,mx,my,mz):
    pauli_i = np.eye(2,dtype=np.complex128)
    pauli_x = np.array([[0,1],[1,0]],dtype=np.complex128)
    pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
    pauli_z = np.array([[1,0],[0,-1]],dtype=np.complex128)

    # Prepare initial Hamiltonian and boundary operator.
    H = -mx*pauli_x - my*pauli_y - mz*pauli_z
    Bx = pauli_x
    By = pauli_y
    Bz = pauli_z

    # Prepare the initial Hamiltonian wave.
    H1 = H*0.

    for l in range(1,L):
        #print(f'Adding spin {l} (dimension is {len(H)})')
        # Construct expanded operators.
        # Spins on the new site
        X_ = np.kron(np.eye(len(H)), pauli_x)
        Y_ = np.kron(np.eye(len(H)), pauli_y)
        Z_ = np.kron(np.eye(len(H)), pauli_z)

        # The spin that used to be on the right-hand boundary
        Bx = np.kron(Bx, np.eye(2))
        By = np.kron(By, np.eye(2))
        Bz = np.kron(Bz, np.eye(2))
       
        # Expand all operators
        H = np.kron(H, np.eye(2))
        H1 = np.kron(H1, np.eye(2))
        # Magnetic field on the new site
        H -= mx*X_ + my*Y_ + mz*Z_
        H1 -= (mx*X_ + my*Y_ + mz*Z_)*np.sin(2*np.pi*l / L)
        # Couple the new site to the old
        H -= Jx*X_@Bx + Jy*Y_@By + Jz*Z_@Bz
        H1 -= (Jx*X_@Bx + Jy*Y_@By + Jz*Z_@Bz)*np.sin(2*np.pi*(l-0.5)/L)

        # Project onto the lowest D states.
        vals, vecs = np.linalg.eigh(H)
        def project(op):
            return (vecs.conj().T @ op @ vecs)[:D,:D]
        Bx = project(X_)
        By = project(Y_)
        Bz = project(Z_)
        H = project(H)
        H1 = project(H1)

    return {'H': H, 'H1': H1}

def hamiltonian(L,D,Jx,Jy,Jz,mx,my,mz):
    return operators(L,D,Jx,Jy,Jz,mx,my,mz)['H']

def hamiltonian_wave(L,D,Jx,Jy,Jz,mx,my,mz):
    return operators(L,D,Jx,Jy,Jz,mx,my,mz)['H1']

def partition(L,D,T,Jx,Jy,Jz,mx,my,mz):
    H = hamiltonian(L,D,Jx,Jy,Jz,mx,my,mz)
    vals, vecs = np.linalg.eigh(H)
    return np.sum(np.exp(-vals/T))

def free_energy(L,D,T,Jx,Jy,Jz,mx,my,mz):
    H = hamiltonian(L,D,Jx,Jy,Jz,mx,my,mz)
    vals, vecs = np.linalg.eigh(H)
    return -T*sps.logsumexp(-vals/T)

if __name__ == '__main__':
    import sys

    Jx = float(sys.argv[1])
    Jy = float(sys.argv[2])
    Jz = float(sys.argv[3])
    mx = float(sys.argv[4])
    my = float(sys.argv[5])
    mz = float(sys.argv[6])

    # Size of lattice.
    L = int(sys.argv[7])

    # Dimension of truncation.
    D = int(sys.argv[8])

    # The temperature is assumed to be one (as it is degenerate with the overall
    # normalization of the Hamiltonian).
    print(free_energy(L,D,1.,Jx,Jy,Jz,mx,my,mz))

