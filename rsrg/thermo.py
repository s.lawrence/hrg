#!/usr/bin/env python

import numpy as np

import matplotlib.pyplot as plt

import heisenberg
import ising
import scalar

L = 20

Ds = [3,5,10,20]
mus = np.linspace(0.5,1.5,31)

plt.figure()
for D in Ds:
    Fs = []
    for mu in mus:
        F = ising.free_energy(L,D,1e-1,1.,mu)
        Fs.append(F)
    plt.plot(mus, Fs, label=f'D={D}')
plt.legend(loc='best')
plt.tight_layout()
plt.show()
