#!/usr/bin/env python

"""
HRG of one scalar field.
"""

import numpy as np
import scipy.special as sps

def operators(L,D,m2,lamda):
    pass

def hamiltonian(L,D,m2,lamda):
    return operators(L,D,m2,lamda)['H']

def hamiltonian_wave(L,D,m2,lamda):
    return operators(L,D,m2,lamda)['H1']

def partition(L,D,T,m2,lamda):
    H = hamiltonian(L,D,m2,lamda)
    vals, vecs = np.linalg.eigh(H)
    return np.sum(np.exp(-vals/T))

def free_energy(L,D,T,m2,lamda):
    H = hamiltonian(L,D,m2,lamda)
    vals, vecs = np.linalg.eigh(H)
    return -T*sps.logsumexp(-vals/T)

if __name__ == '__main__':
    import sys

    m2 = float(sys.argv[1])
    lamda = float(sys.argv[2])

    # Size of lattice.
    L = int(sys.argv[3])

    # Temperature
    T = float(sys.argv[4])

    # Dimension of truncation.
    D = int(sys.argv[5])

    print(free_energy(L,D,T,m2,lamda))

