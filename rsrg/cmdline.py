#!/usr/bin/env python

import importlib

def _numeric(s):
    try:
        return int(s)
    except:
        return float(s)

def make_model(args):
    model_name = args[0]
    model = importlib.import_module(model_name)
    return model, [_numeric(s) for s in args[1:]]
