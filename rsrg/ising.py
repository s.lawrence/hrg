#!/usr/bin/env python

"""
The transverse Ising model in one dimension. This is a special case of the
Heisenberg model, implemented here for convenience.
"""

import heisenberg

def operators(L,D,J,mu):
    return heisenberg.operators(L,D,0,0,J,mu,0,0)

def hamiltonian(L,D,J,mu):
    return heisenberg.hamiltonian(L,D,0,0,J,mu,0,0)

def hamiltonian_wave(L,D,J,mu):
    return heisenberg.hamiltonian_wave(L,D,0,0,J,mu,0,0)

def partition(L,D,T,J,mu):
    return heisenberg.partition(L,D,T,0,0,J,mu,0,0)

def free_energy(L,D,T,J,mu):
    return heisenberg.free_energy(L,D,T,0,0,J,mu,0,0)

if __name__ == '__main__':
    import sys

    J = float(sys.argv[1])
    mu = float(sys.argv[2])

    # Size of lattice.
    L = int(sys.argv[3])

    # Dimension of truncation.
    D = int(sys.argv[4])

    # The temperature is assumed to be one (as it is degenerate with the overall
    # normalization of the Hamiltonian).
    print(free_energy(L,D,1.,J,mu))

