#!/usr/bin/env python

# Diffusion in the isotropic Heisenberg model.

import sys

import numpy as np

import matplotlib.pyplot as plt

import cmdline
import heisenberg
import ising
import scalar

beta = float(sys.argv[1])
L = int(sys.argv[2])
J = 1
model = heisenberg

dt = 0.02
time = 10.

Ds = [30,50,80]

plt.figure()
for D in Ds:
    ops = model.operators(L, D, J, J, J, 0, 0, 0)
    H, H1 = ops['H'], ops['H1']
    vals, vecs = np.linalg.eigh(H)
    rho = vecs @ np.diag(np.exp(-beta * vals)) @ vecs.conj().T
    U_dt = vecs @ np.diag(np.exp(-1j * dt * vals)) @ vecs.conj().T
    U = np.eye(H.shape[0])
    t = 0.
    times = []
    corrs = []
    while t < time:
        t += dt
        U = U @ U_dt
        times.append(t)
        corrs.append((rho @ U.conj().T @ H1 @ U @ H1).trace()/rho.trace())
    corrs = np.array(corrs)
    plt.plot(times, corrs.real, label=f'{D}')

plt.legend(loc='best')
plt.tight_layout()
plt.show()
