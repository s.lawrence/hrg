#!/usr/bin/env python

import sys

import numpy as np

import matplotlib.pyplot as plt

import cmdline
import heisenberg
import ising
import scalar

beta = float(sys.argv[1])
model, args = cmdline.make_model(sys.argv[2:])

dt = 0.02
time = 5.

plt.figure()
ops = model.operators(*args)
H, H1 = ops['H'], ops['H1']
vals, vecs = np.linalg.eigh(H)
rho = vecs @ np.diag(np.exp(-beta * vals)) @ vecs.conj().T
U_dt = vecs @ np.diag(np.exp(-1j * dt * vals)) @ vecs.conj().T
U = np.eye(H.shape[0])
t = 0.
times = []
corrs = []
while t < time:
    t += dt
    U = U @ U_dt
    times.append(t)
    corrs.append((rho @ U.conj().T @ H1 @ U @ H1).trace()/rho.trace())
corrs = np.array(corrs)
plt.plot(times, corrs.real)

plt.legend(loc='best')
plt.tight_layout()
plt.show()
