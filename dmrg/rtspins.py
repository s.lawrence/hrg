#!/usr/bin/env python

import numpy as np

pauli_i = np.eye(2,dtype=np.complex128)
pauli_x = np.array([[0,1],[1,0]],dtype=np.complex128)
pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
pauli_z = np.array([[1,0],[0,-1]],dtype=np.complex128)

# Dimension of truncation.
D = 6


