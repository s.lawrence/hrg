#!/usr/bin/env python

"""
Thermodynamics of the transverse Ising model. This is a nice warm-up, as the
only operators we need to track are the Hamiltonian and the right-most spin.
"""

# https://arxiv.org/pdf/cond-mat/0409292.pdf

import sys

import numpy as np

J = 1.
mu = float(sys.argv[1])

pauli_i = np.eye(2,dtype=np.complex128)
pauli_x = np.array([[0,1],[1,0]],dtype=np.complex128)
pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
pauli_z = np.array([[1,0],[0,-1]],dtype=np.complex128)

# Dimension of truncation.
D = 6

# Size of lattice.
L = 8

