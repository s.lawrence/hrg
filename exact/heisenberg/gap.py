#!/usr/bin/env python

"""
Exact diagonalization for the Heisenberg model in 1+1 dimensions.
"""

import sys
import numpy
from scipy.sparse import *
from scipy.sparse.linalg import eigsh

L = int(sys.argv[1])
Jx = float(sys.argv[2])
Jy = float(sys.argv[3])
Jz = float(sys.argv[4])
mu = float(sys.argv[5])

smat = dok_matrix
fmt = 'dok'

pauli_x = smat([[0,1],[1,0]], dtype=numpy.complex128)
pauli_y = smat([[0,-1j],[1j,0]], dtype=numpy.complex128)
pauli_z = smat([[1,0],[0,-1]], dtype=numpy.complex128)

sigma_x = [eye(1)]*L
sigma_y = [eye(1)]*L
sigma_z = [eye(1)]*L

for n in range(L):
    for m in range(L):
        if n == m:
            sigma_x[n] = kron(sigma_x[n], pauli_x, format=fmt)
            sigma_y[n] = kron(sigma_y[n], pauli_y, format=fmt)
            sigma_z[n] = kron(sigma_z[n], pauli_z, format=fmt)
        else:
            sigma_x[n] = kron(sigma_x[n], eye(2), format=fmt)
            sigma_y[n] = kron(sigma_y[n], eye(2), format=fmt)
            sigma_z[n] = kron(sigma_z[n], eye(2), format=fmt)

H = smat(sigma_x[0].shape, dtype=numpy.complex128)

for n in range(L):
    H -= mu*sigma_x[n]
    np = (n+1)%L
    H -= Jx*sigma_x[n]@sigma_x[np]
    H -= Jy*sigma_y[n]@sigma_y[np]
    H -= Jz*sigma_z[n]@sigma_z[np]

vals, vecs = eigsh(H, k=L+2, which='SA')
vals = numpy.sort(vals)
gnd = vals[0]
print(vals[vals - gnd > 1e-2][0] - gnd)
