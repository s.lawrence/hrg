#!/usr/bin/env python

"""
Exact diagonalization for the transverse Ising model in 1+1 dimensions.
"""

import sys
from numpy import *
import numpy.random as random
from scipy.special import logsumexp

L = int(sys.argv[1])
mu = float(sys.argv[2])
J = 1.
beta = float(sys.argv[3])

pauli_x = array([[0,1],[1,0]])
pauli_z = array([[1,0],[0,-1]])

sigma_x = [eye(1)]*L
sigma_z = [eye(1)]*L

for n in range(L):
    for m in range(L):
        if n == m:
            sigma_x[n] = kron(sigma_x[n], pauli_x)
            sigma_z[n] = kron(sigma_z[n], pauli_z)
        else:
            sigma_x[n] = kron(sigma_x[n], eye(2))
            sigma_z[n] = kron(sigma_z[n], eye(2))

H = zeros(sigma_x[0].shape)


for n in range(L):
    H -= mu*sigma_x[n]
    np = (n+1)%L
    H -= J*sigma_z[n]@sigma_z[np]

vals, vecs = linalg.eigh(H)
print(-logsumexp(-vals))

rho = vecs @ diag(exp(-beta*vals)) @ vecs.conj().T
print((rho @ sigma_x[int(L/2)-1]).trace() / rho.trace())

