#!/usr/bin/env python

"""
Exact diagonalization for the transverse Ising model in 1+1 dimensions.
Computation of diffusion.
"""

import sys
from numpy import *
import numpy.random as random
from scipy.special import logsumexp

L = int(sys.argv[1])
mu = float(sys.argv[2])
J = 1.
beta = float(sys.argv[3])

pauli_x = array([[0,1],[1,0]])
pauli_z = array([[1,0],[0,-1]])

sigma_x = [eye(1)]*L
sigma_z = [eye(1)]*L

for n in range(L):
    for m in range(L):
        if n == m:
            sigma_x[n] = kron(sigma_x[n], pauli_x)
            sigma_z[n] = kron(sigma_z[n], pauli_z)
        else:
            sigma_x[n] = kron(sigma_x[n], eye(2))
            sigma_z[n] = kron(sigma_z[n], eye(2))

H = zeros(sigma_x[0].shape)
for n in range(L):
    H -= mu*sigma_x[n]
    np = n+1
    if np < L:
        H -= J*sigma_z[n]@sigma_z[np]

Hk = zeros(sigma_x[0].shape)
for n in range(L):
    Hk -= mu*sigma_x[n] * sin(2*pi*n/L)
    np = n+1
    if np < L:
        Hk -= J*sigma_z[n]@sigma_z[np] * sin(2*pi*(n+0.5)/L)


vals, vecs = linalg.eigh(H)
rho = vecs @ diag(exp(-beta * vals)) @ vecs.conj().T


h = 5e-3
U_h = vecs @ diag(exp(-1j * h * vals)) @ vecs.conj().T
U = eye(sigma_x[0].shape[0])
time = 0
times = []
corrs = []
while time < 1.:
    time += h
    U = U @ U_h
    times.append(time)
    corrs.append((rho @ U.conj().T @ Hk @ U @ Hk).trace()/rho.trace())
corrs = array(corrs)

import matplotlib.pyplot as plt
plt.plot(times,corrs.real, label='real')
plt.plot(times,corrs.imag, label='imag')
plt.legend(loc='best')
plt.tight_layout()
plt.show()
